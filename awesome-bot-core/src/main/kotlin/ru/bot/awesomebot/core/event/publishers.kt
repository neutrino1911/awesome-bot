package ru.bot.awesomebot.core.event

import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import ru.bot.awesomebot.core.config.TelegramBotProperties

interface MessagePublisher {
    fun publish(message: Message)
}

interface UpdatePublisher {
    fun publish(update: Update)
}

@Component
class CommandEventPublisher(
    private val properties: TelegramBotProperties,
    private val publisher: ApplicationEventPublisher,
) : MessagePublisher {
    override fun publish(message: Message) {
        val text = (message.text ?: message.caption ?: "").substringBefore(' ')
        if (text.startsWith("/")) {
            val command = getCommand(text) ?: return
            publisher.publishEvent(CommandEvent(this, message, command))
        }
    }

    private fun getCommand(text: String): Command? {
        val parts = text.trim().split(' ')[0].split('@')
        val direct = parts.size == 2
        if (direct && parts[1] != properties.identifier) return null
        val command = parts[0].replace('_', '/')
        return Command(command, direct)
    }
}

@Component
class MemberLeftChatEventPublisher(private val publisher: ApplicationEventPublisher) : UpdatePublisher {
    override fun publish(update: Update) {
        if (update.message?.leftChatMember != null) {
            publisher.publishEvent(MemberLeftChatEvent(this, update))
        }
    }
}

@Component
class MessageEventPublisher(private val publisher: ApplicationEventPublisher) : MessagePublisher {
    override fun publish(message: Message) = publisher.publishEvent(MessageEvent(this, message))
}

@Component
class NewChatMembersEventPublisher(private val publisher: ApplicationEventPublisher) : UpdatePublisher {
    override fun publish(update: Update) {
        if (!update.message?.newChatMembers.isNullOrEmpty()) {
            publisher.publishEvent(NewChatMembersEvent(this, update))
        }
    }
}

@Component
class UpdateEventPublisher(private val publisher: ApplicationEventPublisher) : UpdatePublisher {
    override fun publish(update: Update) = publisher.publishEvent(UpdateEvent(this, update))
}
