package ru.bot.awesomebot.core.aop

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import ru.bot.awesomebot.core.InsufficientPermissionException
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.util.isAdmin

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class AdminOnly

@Aspect
@Component
@Order(-10)
class AdminOnlyAdvice(
    private val bot: TelegramBot,
    private val extractor: UserChatRefExtractor,
) {
    @Around("@annotation(ru.bot.awesomebot.core.aop.AdminOnly) || @within(ru.bot.awesomebot.core.aop.AdminOnly)")
    fun around(pjp: ProceedingJoinPoint): Any? {
        val (chatId, userId) = extractor.extract(pjp) ?: return pjp.proceed()
        return if (bot.isAdmin(chatId, userId)) {
            pjp.proceed()
        } else {
            throw InsufficientPermissionException("[Ошибка]")
        }
    }
}
