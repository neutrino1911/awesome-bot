package ru.bot.awesomebot.core.dispatching

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.CallbackQuery
import org.telegram.telegrambots.meta.api.objects.InaccessibleMessage
import org.telegram.telegrambots.meta.api.objects.MaybeInaccessibleMessage
import org.telegram.telegrambots.meta.api.objects.Message
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.jvm.javaType

@Component
class RequestParamExtractor {

    fun extractParam(query: CallbackQuery, param: KParameter, params: Map<String, String>): Any {
        val metadata = extractParamMetadata(param)

        return tryPredefinedParams(query, metadata)
            ?: extractQueryParam(metadata, params)
    }

    fun extractParam(message: Message, param: KParameter, params: Map<String, String>): Any {
        val metadata = extractParamMetadata(param)

        return tryPredefinedParams(message, metadata)
            ?: extractQueryParam(metadata, params)
    }

    private fun extractParamMetadata(param: KParameter): ParamMetadata {
        val paramName = (param.findAnnotation<CommandParam>()?.name ?: "").ifBlank { param.name!! }

        return ParamMetadata(paramName, param.type, param.type.javaType as Class<*>)
    }

    private fun tryPredefinedParams(query: CallbackQuery, metadata: ParamMetadata): Any? {
        return when (metadata.clazz) {
            CallbackQuery::class.java -> query
            else -> tryPredefinedParams(query.message, metadata)
        }
    }

    private fun tryPredefinedParams(message: MaybeInaccessibleMessage, metadata: ParamMetadata): Any? {
        return when (message) {
            is InaccessibleMessage -> null

            is Message -> when (metadata.clazz) {
                Message::class.java -> message
                CmdArgs::class.java -> message.text.split("\\s+".toRegex()).drop(1).toCmdArgs()
                else -> null
            }

            else -> null
        }
    }

    private fun extractQueryParam(metadata: ParamMetadata, params: Map<String, String>): Any {
        val paramName = metadata.name
        val stringValue = params[paramName] ?: error("Query parameter [$paramName] not found")

        return when (metadata.clazz) {
            String::class.java -> stringValue
            Int::class.java -> stringValue.toInt()
            Long::class.java -> stringValue.toLong()
            Boolean::class.java -> stringValue.toBoolean()
            else -> error("Unsupported type: ${metadata.type}")
        }
    }

    private fun List<String>.toCmdArgs() = CmdArgs(this)

    private data class ParamMetadata(
        val name: String,
        val type: KType,
        val clazz: Class<*>,
    )

}
