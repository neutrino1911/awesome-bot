package ru.bot.awesomebot.core.event

import org.springframework.context.ApplicationEvent
import org.telegram.telegrambots.meta.api.objects.CallbackQuery
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.inlinequery.ChosenInlineQuery
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery

interface HasMessage {
    val message: Message
}

interface HasUpdate {
    val update: Update
}

interface HasCallbackQuery {
    val callbackQuery: CallbackQuery
}

interface HasInlineQuery {
    val inlineQuery: InlineQuery
}

interface HasChosenInlineQuery {
    val chosenInlineQuery: ChosenInlineQuery
}

class CommandEvent(source: Any, override val message: Message, val command: Command) : HasMessage, ApplicationEvent(source)

data class Command(val command: String, val direct: Boolean)

class MessageEvent(source: Any, override val message: Message) : HasMessage, ApplicationEvent(source)

class UpdateEvent(source: Any, override val update: Update) : HasUpdate, ApplicationEvent(source)

class MemberLeftChatEvent(source: Any, override val update: Update) : HasUpdate, ApplicationEvent(source)

class NewChatMembersEvent(source: Any, override val update: Update) : HasUpdate, ApplicationEvent(source)

class CallbackQueryEvent(source: Any, override val callbackQuery: CallbackQuery) : HasCallbackQuery, ApplicationEvent(source)
