package ru.bot.awesomebot.core.config

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["ru.bot.awesomebot.core"])
@EnableConfigurationProperties(TelegramBotProperties::class)
class TelegramBotAutoconfiguration
