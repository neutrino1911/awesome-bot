package ru.bot.awesomebot.core.extraction

import org.aspectj.lang.ProceedingJoinPoint
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import ru.bot.awesomebot.core.event.HasMessage
import ru.bot.awesomebot.core.event.HasUpdate

interface UserIdExtractor<T> {
    fun extractUserId(pjp: ProceedingJoinPoint, chain: UserIdExtractorChain<T>): Long?
    fun extractUserId(target: T): Long?
}

interface UserIdExtractorChain<T> {
    fun extractUserId(pjp: ProceedingJoinPoint): Long?
}

interface ChatIdExtractor<T> {
    fun extractChatId(pjp: ProceedingJoinPoint, chain: ChatIdExtractorChain<T>): Long?
    fun extractChatId(target: T): Long?
}

interface ChatIdExtractorChain<T> {
    fun extractChatId(pjp: ProceedingJoinPoint): Long?
}

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
class VirtualUserIdExtractorChain<T>(private val extractors: List<UserIdExtractor<T>>) : UserIdExtractorChain<T> {
    private var currentPosition = 0
    override fun extractUserId(pjp: ProceedingJoinPoint): Long? {
        return if (currentPosition == extractors.size) null
        else {
            currentPosition++
            extractors[currentPosition - 1].extractUserId(pjp, this)
        }
    }
}

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
class VirtualChatIdExtractorChain<T>(private val extractors: List<ChatIdExtractor<T>>) : ChatIdExtractorChain<T> {
    private var currentPosition = 0
    override fun extractChatId(pjp: ProceedingJoinPoint): Long? {
        return if (currentPosition == extractors.size) null
        else {
            currentPosition++
            extractors[currentPosition - 1].extractChatId(pjp, this)
        }
    }
}

@Component
class MessageExtractor : UserIdExtractor<Message>, ChatIdExtractor<Message> {
    override fun extractUserId(pjp: ProceedingJoinPoint, chain: UserIdExtractorChain<Message>) = extract(pjp, chain)
    override fun extractUserId(target: Message): Long? = target.from.id
    override fun extractChatId(pjp: ProceedingJoinPoint, chain: ChatIdExtractorChain<Message>) = extract(pjp, chain)
    override fun extractChatId(target: Message): Long? = target.chatId
}

@Component
class UpdateExtractor(private val messageExtractor: MessageExtractor) : UserIdExtractor<Update>, ChatIdExtractor<Update> {
    override fun extractUserId(pjp: ProceedingJoinPoint, chain: UserIdExtractorChain<Update>) = extract(pjp, chain)
    override fun extractUserId(target: Update): Long? = target.message?.let { messageExtractor.extractUserId(it) }
    override fun extractChatId(pjp: ProceedingJoinPoint, chain: ChatIdExtractorChain<Update>) = extract(pjp, chain)
    override fun extractChatId(target: Update): Long? = target.message?.let { messageExtractor.extractChatId(it) }
}

@Component
class HasMessageExtractor(private val extractor: MessageExtractor) : UserIdExtractor<HasMessage>, ChatIdExtractor<HasMessage> {
    override fun extractUserId(pjp: ProceedingJoinPoint, chain: UserIdExtractorChain<HasMessage>) = extract(pjp, chain)
    override fun extractUserId(target: HasMessage): Long? = target.message.let { extractor.extractUserId(it) }
    override fun extractChatId(pjp: ProceedingJoinPoint, chain: ChatIdExtractorChain<HasMessage>) = extract(pjp, chain)
    override fun extractChatId(target: HasMessage): Long? = target.message.let { extractor.extractChatId(it) }
}

@Component
class HasUpdateExtractor(private val extractor: UpdateExtractor) : UserIdExtractor<HasUpdate>, ChatIdExtractor<HasUpdate> {
    override fun extractUserId(pjp: ProceedingJoinPoint, chain: UserIdExtractorChain<HasUpdate>) = extract(pjp, chain)
    override fun extractUserId(target: HasUpdate): Long? = target.update.let { extractor.extractUserId(it) }
    override fun extractChatId(pjp: ProceedingJoinPoint, chain: ChatIdExtractorChain<HasUpdate>) = extract(pjp, chain)
    override fun extractChatId(target: HasUpdate): Long? = target.update.let { extractor.extractChatId(it) }
}

inline fun <reified T> ProceedingJoinPoint.firstOfType(): T? = args.filterIsInstance<T>().firstOrNull()

inline fun <reified T> UserIdExtractor<T>.extract(pjp: ProceedingJoinPoint, chain: UserIdExtractorChain<T>): Long? {
    return pjp.firstOfType<T>()
        ?.let { extractUserId(it) }
        ?: chain.extractUserId(pjp)
}

inline fun <reified T> ChatIdExtractor<T>.extract(pjp: ProceedingJoinPoint, chain: ChatIdExtractorChain<T>): Long? {
    return pjp.firstOfType<T>()
        ?.let { extractChatId(it) }
        ?: chain.extractChatId(pjp)
}
