package ru.bot.awesomebot.core.component

import org.springframework.aop.support.AopUtils
import org.springframework.beans.factory.getBeansWithAnnotation
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeAllChatAdministrators
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.aop.AdminOnly
import ru.bot.awesomebot.core.dispatching.CommandMapping

@Component
class BotCommandsSetter(
    private val telegramBot: TelegramBot,
) {
    @EventListener(ContextRefreshedEvent::class)
    fun onContextRefreshedEvent(event: ContextRefreshedEvent) {
        val context = event.applicationContext

        val commands = context.beanDefinitionNames
            .mapNotNull { context.getBean(it) }
            .map { AopUtils.getTargetClass(it) }
            .flatMap { clazz -> clazz.declaredMethods.map { clazz to it } }
            .filter { it.second.isAnnotationPresent(CommandMapping::class.java) }
            .flatMap { (clazz, method) ->
                val ann = method.getDeclaredAnnotation(CommandMapping::class.java)
                ann.path.map { path ->
                    val command = path.substring(1).replace('/', '_')
                    val adminOnly = method.isAnnotationPresent(AdminOnly::class.java) || clazz.isAnnotationPresent(AdminOnly::class.java)
                    val description = (if (adminOnly) "[admin] " else "") +
                            (ann.description.takeUnless { it.isBlank() } ?: command.replace('_', ' ').replaceFirstChar { it.uppercaseChar() })
                    Command(
                        name = command,
                        description = description,
                        adminOnly = adminOnly,
                    )
                }
            }
            .sortedBy { it.name }


        val sharedCommands = commands.filter { !it.adminOnly }.map()
        val adminCommands = commands.filter { it.adminOnly }.map()

        telegramBot.execute(SetMyCommands(sharedCommands, BotCommandScopeDefault(), "en"))
        telegramBot.execute(SetMyCommands(sharedCommands + adminCommands, BotCommandScopeAllChatAdministrators(), "en"))
    }

    private data class Command(val name: String, val description: String, val adminOnly: Boolean)

    private fun Iterable<Command>.map(): List<BotCommand> {
        return map { BotCommand(it.name, it.description) }
    }
}
