package ru.bot.awesomebot.core.util

import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.IOUtils
import org.telegram.telegrambots.meta.api.methods.GetFile
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatAdministrators
import org.telegram.telegrambots.meta.api.methods.send.SendDocument
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto
import org.telegram.telegrambots.meta.api.objects.InputFile
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException
import ru.bot.awesomebot.core.TelegramBot
import java.io.ByteArrayInputStream
import java.net.URL

fun TelegramBot.getFileHash(fileId: String, token: String): String {
    val getFile = GetFile()
    getFile.fileId = fileId
    val res = execute(getFile)
    val url = URL(res.getFileUrl(token))
    val bytes = IOUtils.toByteArray(url)
    return DigestUtils.sha512Hex(bytes)
}

fun TelegramBot.sendPhoto(message: Message, filename: String, bytes: ByteArray) {
    val chatId = message.chatId
    try {
        execute(SendPhoto().apply {
            this.chatId = chatId.toString()
            replyToMessageId = message.messageId
            disableNotification()
            photo = InputFile(ByteArrayInputStream(bytes), filename)
        })
    } catch (e1: TelegramApiRequestException) {
        try {
            execute(SendDocument().apply {
                this.chatId = chatId.toString()
                replyToMessageId = message.messageId
                document = InputFile(ByteArrayInputStream(bytes), filename)
            })
        } catch (e2: TelegramApiRequestException) {
            execute(SendMessage().apply {
                this.chatId = chatId.toString()
                replyToMessageId = message.messageId
                text = "Не могу отправить файл :("
            })
        }
    }
}

fun TelegramBot.isFromAdmin(message: Message): Boolean {
    return message.isUserMessage || isAdmin(message.chatId, message.from.id)
}

fun TelegramBot.isAdmin(chatId: Long, userId: Long): Boolean {
    if (chatId == userId) return true
    val getChatAdministrators = GetChatAdministrators().apply {
        this.chatId = chatId.toString()
    }
    return execute(getChatAdministrators).any { it.user.id == userId }
}
