package ru.bot.awesomebot.component.handler

import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import ru.bot.awesomebot.service.AdviceService

@ExtendWith(MockKExtension::class)
class AdviceHandlerTest(
    @RelaxedMockK
    private val adviceService: AdviceService,
) {

    private val adviceHandler = AdviceHandler(adviceService)

    @ParameterizedTest
    @CsvSource(
        quoteCharacter = '"', textBlock = """
        "Не сиди, блять! Сделай что-нибудь!", "Не сиди, блять! Сделай что-нибудь!"
        "Не сиди блять! Сделай что-нибудь!",  "Не сиди, блять! Сделай что-нибудь!"
        "Готовь блять подарок заранее!",      "Готовь, блять, подарок заранее!"
        "Готовь, блять подарок заранее!",     "Готовь, блять, подарок заранее!"
        "Готовь блять, подарок заранее!",     "Готовь, блять, подарок заранее!"
        "Готовь, блять, подарок заранее!",    "Готовь, блять, подарок заранее!" """
    )
    fun wrapBlyat(src: String, expected: String) {
        val actual = adviceHandler.wrapBlyat(src)
        assertThat(actual).isEqualTo(expected)
    }

}
