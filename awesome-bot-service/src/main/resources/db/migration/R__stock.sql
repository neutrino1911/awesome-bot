CREATE TABLE IF NOT EXISTS stock
(
    chat_id BIGINT  NOT NULL,
    key     VARCHAR NOT NULL,
    ticker  VARCHAR NOT NULL,
    figi    VARCHAR NOT NULL,

    CONSTRAINT stock_pk PRIMARY KEY (chat_id, key),
    CONSTRAINT stock_chat_fk FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE
);

ALTER TABLE stock
    ADD COLUMN IF NOT EXISTS figi VARCHAR;
