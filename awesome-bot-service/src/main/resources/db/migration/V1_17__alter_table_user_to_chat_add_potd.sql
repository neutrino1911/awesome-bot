ALTER TABLE user_to_chat
    ADD COLUMN is_potd_participant BOOLEAN NOT NULL DEFAULT FALSE;