CREATE TABLE IF NOT EXISTS potd_result
(
  id      BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
  chat_id BIGINT                              NOT NULL,
  user_id BIGINT                              NOT NULL,
  "date"  DATE                                NOT NULL,
  CONSTRAINT potd_result_pk PRIMARY KEY (id),
  CONSTRAINT potd_result_chat_fk FOREIGN KEY (chat_id) REFERENCES chat (id) ON DELETE CASCADE,
  CONSTRAINT potd_result_user_fk FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE
);
