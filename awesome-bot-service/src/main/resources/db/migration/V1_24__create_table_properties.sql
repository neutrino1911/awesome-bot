CREATE TABLE IF NOT EXISTS properties (
    name    VARCHAR CONSTRAINT properties_pk PRIMARY KEY,
    "value" VARCHAR NOT NULL
);
