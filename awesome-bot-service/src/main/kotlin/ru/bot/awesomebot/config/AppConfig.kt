package ru.bot.awesomebot.config

import feign.codec.Encoder
import feign.form.FormEncoder
import org.springframework.beans.factory.ObjectFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.boot.autoconfigure.http.HttpMessageConverters
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.cloud.openfeign.support.SpringEncoder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Scope
import org.springframework.context.event.ApplicationEventMulticaster
import org.springframework.context.event.SimpleApplicationEventMulticaster
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

@Configuration
@EnableScheduling
@EnableFeignClients(basePackages = ["ru.bot.awesomebot.service"])
@EnableJpaRepositories(basePackages = ["ru.bot.awesomebot.data.repository"])
class AppConfig {

    @Bean(name = ["asyncTaskExecutor"])
    fun asyncTaskExecutor(): ThreadPoolExecutor {
        return ThreadPoolExecutor(4, 20, 60L, TimeUnit.SECONDS, LinkedBlockingQueue())
    }

    @Bean(name = ["applicationEventMulticaster"])
    fun simpleApplicationEventMulticaster(
        @Qualifier("asyncTaskExecutor") asyncTaskExecutor: ThreadPoolExecutor,
    ): ApplicationEventMulticaster {
        return SimpleApplicationEventMulticaster().apply {
            setTaskExecutor(asyncTaskExecutor)
        }
    }

    @Bean
    @Primary
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    fun feignFormEncoder(messageConverters: ObjectFactory<HttpMessageConverters?>?): Encoder {
        return FormEncoder(SpringEncoder(messageConverters))
    }
}
