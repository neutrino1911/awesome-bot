package ru.bot.awesomebot.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration(proxyBeanMethods = false)
class WebClientConfig {

    @Bean("moexWebClient")
    fun moexWebClient(builder: WebClient.Builder): WebClient {
        return builder.baseUrl("https://iss.moex.com/iss").build()
    }
}
