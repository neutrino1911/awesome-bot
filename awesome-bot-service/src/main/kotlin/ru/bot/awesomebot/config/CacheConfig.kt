package ru.bot.awesomebot.config

import com.github.benmanes.caffeine.cache.Caffeine
import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cache.caffeine.CaffeineCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Duration

@EnableCaching
@Configuration(proxyBeanMethods = false)
class CacheConfig {

    @Bean
    fun cacheManagerCustomizer(): CacheManagerCustomizer<CaffeineCacheManager> {
        return CacheManagerCustomizer { cacheManager ->
            with(cacheManager) {
                addCache("moex_history") { maximumSize(1000) }
                addCache("moex_current") {
                    maximumSize(100)
                    expireAfterWrite(Duration.ofMinutes(5))
                }
            }
        }
    }

    private fun CaffeineCacheManager.addCache(name: String, block: Caffeine<Any, Any>.() -> Unit) {
        registerCustomCache(name, Caffeine.newBuilder().apply(block).build())
    }
}
