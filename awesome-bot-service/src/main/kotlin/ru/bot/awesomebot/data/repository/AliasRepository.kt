package ru.bot.awesomebot.data.repository

import org.springframework.data.repository.CrudRepository
import ru.bot.awesomebot.data.entity.Alias
import ru.bot.awesomebot.data.entity.AliasId

interface AliasRepository : CrudRepository<Alias, AliasId> {
    fun findAllByChatId(chatId: Long): List<Alias>
}
