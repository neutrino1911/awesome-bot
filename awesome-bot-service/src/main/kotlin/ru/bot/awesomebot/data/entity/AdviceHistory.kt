package ru.bot.awesomebot.data.entity

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "advice_history")
data class AdviceHistory(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Column(name = "advice_id", updatable = false)
    val adviceId: Int,

    @Column(name = "user_id", updatable = false)
    val userId: Long,

    @Column(name = "date", insertable = false, updatable = false)
    val date: LocalDate,

    @Column(name = "text", updatable = false)
    val text: String,
)
