package ru.bot.awesomebot.data

import ru.bot.awesomebot.data.entity.AdviceHistory

data class Advice(val id: Int, val text: String)

fun AdviceHistory.toAdvice() = Advice(adviceId, text)
