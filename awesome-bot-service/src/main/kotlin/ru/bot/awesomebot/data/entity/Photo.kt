package ru.bot.awesomebot.data.entity

data class Photo(
    val fileId: String,
    val fileUniqueId: String,
    val width: Int,
    val height: Int,
    val fileSize: Int? = null,
    val filePath: String? = null,
)
