package ru.bot.awesomebot.data.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "user")
data class User(
    @Id
    val id: Long,

    @Column(name = "first_name")
    val firstName: String,

    @Column(name = "last_name")
    val lastName: String? = null,

    @Column(name = "user_name")
    val username: String? = null,

    @Column(name = "is_bot")
    val bot: Boolean = false,
)
