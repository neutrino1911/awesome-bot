package ru.bot.awesomebot.data.repository

import org.springframework.data.repository.CrudRepository
import ru.bot.awesomebot.data.entity.PotdResult
import java.time.LocalDate

interface PotdResultRepository : CrudRepository<PotdResult, Long> {
    fun countByChatIdAndUserId(chatId: Long, userId: Long): Long
    fun findByChatIdAndDate(chatId: Long, date: LocalDate): PotdResult?
}
