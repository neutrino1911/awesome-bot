package ru.bot.awesomebot.component.handler

import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.event.MessageEvent
import ru.bot.awesomebot.core.util.sendMessage
import java.math.BigDecimal
import java.math.RoundingMode

@Component
class DumbassToHumanConverterHandler(
    private val bot: TelegramBot,
) {
    private val regex = """(?:^|\v|\s|\h)([+-]?\d+(?:\.\d+)?)(?:\u00b0|\v|\s|\h)*?(f|c|lb|kg)\b""".toRegex()

    @EventListener(MessageEvent::class)
    fun handle(event: MessageEvent) {
        event.message.text?.let { text ->
            regex.find(text.lowercase())?.let { res ->
                val src = res.groupValues[1]

                when (res.groupValues[2]) {
                    "f" -> bot.sendMessage(event.message, "$src\u00b0F ~= ${fc(src)}\u00b0C")
                    "c" -> bot.sendMessage(event.message, "$src\u00b0C ~= ${cf(src)}\u00b0F")
                    "lb" -> bot.sendMessage(event.message, "$src lb ~= ${lbkg(src)} kg")
                    "kg" -> bot.sendMessage(event.message, "$src kg ~= ${kglb(src)} lb")
                    else -> Unit
                }
            }
        }
    }

    private fun fc(src: String): String {
        return BigDecimal(src).subtract(N_32).divide(N_1_8, 2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString()
    }

    private fun cf(src: String): String {
        return BigDecimal(src).multiply(N_1_8).add(N_32).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString()
    }

    private fun lbkg(src: String): String {
        return BigDecimal(src).multiply(ONE_LB_IN_KG).setScale(2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString()
    }

    private fun kglb(src: String): String {
        return BigDecimal(src).divide(ONE_LB_IN_KG, 2, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString()
    }

    private companion object {
        val N_32 = BigDecimal(32)
        val N_1_8 = BigDecimal("1.8")

        val ONE_LB_IN_KG = BigDecimal("0.453592")
    }
}
