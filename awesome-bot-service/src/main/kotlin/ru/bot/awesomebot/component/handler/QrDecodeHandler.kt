//package ru.bot.awesomebot.component.handler
//
//import com.google.zxing.BinaryBitmap
//import com.google.zxing.MultiFormatReader
//import com.google.zxing.NotFoundException
//import com.google.zxing.client.j2se.BufferedImageLuminanceSource
//import com.google.zxing.common.HybridBinarizer
//import mu.KotlinLogging
//import org.springframework.stereotype.Component
//import org.telegram.telegrambots.meta.api.methods.GetFile
//import org.telegram.telegrambots.meta.api.methods.send.SendMessage
//import org.telegram.telegrambots.meta.api.objects.Message
//import ru.bot.awesomebot.core.TelegramBot
//import ru.bot.awesomebot.core.config.TelegramBotProperties
//import ru.bot.awesomebot.core.dispatching.CommandHandler
//import java.io.IOException
//import java.net.MalformedURLException
//import java.net.URL
//import javax.imageio.ImageIO
//
//private val log = KotlinLogging.logger {}
//
//@Component
//class QrDecodeHandler(
//        private val properties: TelegramBotProperties,
//        private val bot: TelegramBot
//) : CommandHandler {
//
//    override fun getCommand() = "/qrdecode"
//
//    override fun handle(message: Message) {
//        val url = getUrl(message, bot) ?: return
//        val sendMessage = SendMessage()
//        sendMessage.setChatId(message.chatId)
//        sendMessage.replyToMessageId = message.messageId
//        try {
//            val text = decode(url)
//            if (text.isNullOrBlank()) {
//                return
//            }
//            sendMessage.text = text
//        } catch (e: NotFoundException) {
//            sendMessage.text = "There is no QR code in the image"
//        }
//        bot.execute(sendMessage)
//    }
//
//    private fun getUrl(message: Message, bot: TelegramBot): URL? {
//        val list = message.photo
//        if (list.isNullOrEmpty()) {
//            return null
//        }
//        val photo = list.maxByOrNull { it.width }
//        val getFile = GetFile().setFileId(photo!!.fileId)
//        val file = bot.execute(getFile)
//        try {
//            return URL(file.getFileUrl(properties.token))
//        } catch (e: MalformedURLException) {
//            log.error(e.message, e)
//        }
//        return null
//    }
//
//    private fun decode(url: URL): String? {
//        try {
//            val bufferedImage = ImageIO.read(url)
//            val source = BufferedImageLuminanceSource(bufferedImage)
//            val bitmap = BinaryBitmap(HybridBinarizer(source))
//            val result = MultiFormatReader().decode(bitmap)
//            return result.text
//        } catch (e: IOException) {
//            log.error(e.message, e)
//        }
//        return null
//    }
//
//}
