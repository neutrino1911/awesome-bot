package ru.bot.awesomebot.component.event.listener

import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import ru.bot.awesomebot.core.event.MemberLeftChatEvent
import ru.bot.awesomebot.service.UserToChatService

@Component
class MemberLeftChatEventListener(
    private val userToChatService: UserToChatService,
) {
    @EventListener(MemberLeftChatEvent::class)
    fun onApplicationEvent(event: MemberLeftChatEvent) {
        val message = event.update.message
        val member = message.leftChatMember
        userToChatService.deactivate(message.chatId, member.id)
    }
}
