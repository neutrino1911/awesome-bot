package ru.bot.awesomebot.component.event.listener

import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import org.telegram.telegrambots.meta.api.methods.reactions.SetMessageReaction
import org.telegram.telegrambots.meta.api.objects.reactions.ReactionTypeEmoji
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.event.MessageEvent
import ru.bot.awesomebot.data.repository.ChatRepository
import ru.bot.awesomebot.data.repository.UserRepository
import ru.bot.awesomebot.data.toMessage
import ru.bot.awesomebot.service.UserToChatService

@Component
class MessageEventListener(
    private val bot: TelegramBot,
    private val userToChatService: UserToChatService,
    private val userRepository: UserRepository,
    private val chatRepository: ChatRepository,
) {
    private val groupAnonymousBotId = 1087968824L

    @Transactional
    @EventListener(MessageEvent::class)
    fun onApplicationEvent(event: MessageEvent) {
        val message = event.message.toMessage()
        val from = message.from
        val chat = message.chat
        if (!from.bot && from.id != groupAnonymousBotId) {
            userRepository.save(from)
            chatRepository.save(chat)
            userToChatService.addLink(chat.id, from.id)
        }
        if (from.id == 267796463L) {
            bot.execute(
                SetMessageReaction.builder()
                    .chatId(chat.id)
                    .messageId(message.id)
                    .reactionTypes(listOf(ReactionTypeEmoji.builder().emoji("\uD83E\uDD21").build()))
                    .isBig(true)
                    .build()
            )
        }
    }
}
