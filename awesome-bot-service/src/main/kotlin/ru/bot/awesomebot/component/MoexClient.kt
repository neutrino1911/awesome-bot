package ru.bot.awesomebot.component

import arrow.core.Option
import arrow.core.toOption
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalTime

@Component
class MoexClient(
    @Qualifier("moexWebClient")
    val client: WebClient,
) {
    private val log = KotlinLogging.logger {}

    @Cacheable(cacheNames = ["moex_history"])
    fun getHistoryPrice(secid: String, from: LocalDate, till: LocalDate): Option<HistoryResponse> {
        log.warn("getHistoryPrice(secid: $secid, from: $from, till: $till)")
        return client
            .get()
            .uri("/history/engines/currency/markets/selt/boards/cets/securities/$secid.json") {
                it.queryParam("numtrades", 1)
                it.queryParam("from", from)
                it.queryParam("till", till)
                it.queryParam("iss.meta", "off")
                it.queryParam("history.columns", "TRADEDATE,OPEN,LOW,HIGH,CLOSE,NUMTRADES")
                it.queryParam("limit", "100") // default and max
                it.build()
            }
            .retrieve()
            .bodyToMono<HistoryResponse>()
            .block()
            .toOption()
    }

    @Cacheable(cacheNames = ["moex_current"])
    fun getCurrentPrice(secid: String): Option<CurrentResponse> {
        log.warn("getCurrentPrice(secid: $secid)")
        return client
            .get()
            .uri("/engines/currency/markets/selt/boards/cets/securities/$secid.json") {
                it.queryParam("iss.meta", "off")
                it.queryParam("iss.only", "marketdata")
                it.queryParam("marketdata.columns", "UPDATETIME,OPEN,LOW,HIGH,LAST,NUMTRADES")
                it.build()
            }
            .retrieve()
            .bodyToMono<CurrentResponse>()
            .block()
            .toOption()
    }
}

data class HistoryResponse(
    val history: History,
) : Serializable {
    data class History(
        val data: List<Row>,
    ) : Serializable {
        @JsonFormat(shape = JsonFormat.Shape.ARRAY)
        @JsonPropertyOrder
        @JsonIgnoreProperties(ignoreUnknown = true)
        data class Row(
            val date: String,
            val open: BigDecimal,
            val low: BigDecimal,
            val high: BigDecimal,
            val close: BigDecimal,
            val volume: BigDecimal,
        ) : Serializable
    }
}

data class CurrentResponse(
    val marketdata: Marketdata,
) : Serializable {
    data class Marketdata(
        val data: List<Row>,
    ) : Serializable {
        @JsonFormat(shape = JsonFormat.Shape.ARRAY)
        @JsonPropertyOrder
        @JsonIgnoreProperties(ignoreUnknown = true)
        data class Row(
            val time: LocalTime,
            val open: BigDecimal,
            val low: BigDecimal,
            val high: BigDecimal,
            val last: BigDecimal,
            val volume: BigDecimal,
        ) : Serializable
    }
}
