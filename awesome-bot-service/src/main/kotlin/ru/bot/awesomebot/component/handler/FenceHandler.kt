package ru.bot.awesomebot.component.handler

import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.component.PropertiesStorage
import ru.bot.awesomebot.core.TelegramBot
import ru.bot.awesomebot.core.dispatching.CmdArgs
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.event.MessageEvent
import ru.bot.awesomebot.core.util.sendMessage
import kotlin.random.Random

@Component
class FenceHandler(
    private val bot: TelegramBot,
    private val propertiesStorage: PropertiesStorage,
) {
    private val randomProp = "fence.random"

    @CommandMapping("/fence")
    fun fence(message: Message): String? {
        val text = if (message.replyToMessage != null)
            message.replyToMessage.text
        else
            message.text.substringAfter("/fence ")

        return getFence(text)
    }

    @CommandMapping("/fence/random")
    fun fenceRandom(message: Message, args: CmdArgs): String {
        if (args.isEmpty()) {
            return (propertiesStorage.findLong(randomProp, message) ?: 10).toString()
        } else if (args[0].toIntOrNull() != null) {
            val rnd = args[0].toLong()
            if (rnd !in (0..100)) return "Значение должно быть в диапазоне 0..100"
            propertiesStorage.persist(randomProp, message, rnd)
            return "Новое значение установлено"
        } else {
            return "[Ошибка]"
        }
    }

    @EventListener(MessageEvent::class)
    fun messageHandler(event: MessageEvent) {
        event.message.text ?: return
        val rnd = propertiesStorage.findLong(randomProp, event.message) ?: 10
        if (rnd == 0L) return
        if (Random.nextLong(0, 100) <= rnd) {
            getFence(event.message.text)?.let {
                bot.sendMessage(event.message) {
                    this.text = it
                }
            }
        }
    }

    private fun getFence(text: String): String? {
        val charArray = text.toCharArray()
        var counter = 0
        charArray.forEachIndexed { idx, ch ->
            if (ch.lowercaseChar() != ch.uppercaseChar()) {
                if (counter++ % 2 == 0) charArray[idx] = ch.lowercaseChar()
                else charArray[idx] = ch.uppercaseChar()
            }
        }
        if (counter < 3) return null
        return String(charArray)
    }
}
