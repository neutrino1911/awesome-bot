package ru.bot.awesomebot.component

import org.springframework.context.annotation.Primary
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.tools.LongResultSetExtractor
import ru.bot.awesomebot.tools.StringResultSetExtractor
import java.util.concurrent.ConcurrentHashMap

interface PropertiesStorage {
    fun findLong(name: String): Long?
    fun findLong(name: String, chat: Chat) = findLong(name.withChatId(chat))
    fun findLong(name: String, chatId: Long) = findLong(name.withChatId(chatId))
    fun findLong(name: String, message: Message) = findLong(name.withChatId(message))

    fun findString(name: String): String?
    fun findString(name: String, chat: Chat) = findString(name.withChatId(chat))
    fun findString(name: String, chatId: Long) = findString(name.withChatId(chatId))
    fun findString(name: String, message: Message) = findString(name.withChatId(message))

    fun persist(name: String, value: Long)
    fun persist(name: String, chat: Chat, value: Long) = persist(name.withChatId(chat), value)
    fun persist(name: String, chatId: Long, value: Long) = persist(name.withChatId(chatId), value)
    fun persist(name: String, message: Message, value: Long) = persist(name.withChatId(message), value)

    fun persist(name: String, value: String)
    fun persist(name: String, chat: Chat, value: String) = persist(name.withChatId(chat), value)
    fun persist(name: String, chatId: Long, value: String) = persist(name.withChatId(chatId), value)
    fun persist(name: String, message: Message, value: String) = persist(name.withChatId(message), value)

    private fun String.withChatId(message: Message) = withChatId(message.chat)
    private fun String.withChatId(chat: Chat) = withChatId(chat.id)
    private fun String.withChatId(chatId: Long) = "$this.$chatId"
}

@Primary
@Component
class CompositePropertiesStorage(
    private val databasePropertiesStorage: DatabasePropertiesStorage,
    private val inMemoryPropertiesStorage: InMemoryPropertiesStorage,
) : PropertiesStorage {
    override fun findLong(name: String): Long? {
        return inMemoryPropertiesStorage.findLong(name)
            ?: databasePropertiesStorage.findLong(name)?.also {
                inMemoryPropertiesStorage.persist(name, it)
            }
    }

    override fun findString(name: String): String? {
        return inMemoryPropertiesStorage.findString(name)
            ?: databasePropertiesStorage.findString(name)?.also {
                inMemoryPropertiesStorage.persist(name, it)
            }
    }

    override fun persist(name: String, value: Long) {
        inMemoryPropertiesStorage.persist(name, value)
        databasePropertiesStorage.persist(name, value)
    }

    override fun persist(name: String, value: String) {
        inMemoryPropertiesStorage.persist(name, value)
        databasePropertiesStorage.persist(name, value)
    }
}

@Component
class InMemoryPropertiesStorage : PropertiesStorage {
    private val longStorage = ConcurrentHashMap<String, Long>()
    private val stringStorage = ConcurrentHashMap<String, String>()

    override fun findLong(name: String): Long? {
        return longStorage[name]
    }

    override fun findString(name: String): String? {
        return stringStorage[name]
    }

    override fun persist(name: String, value: Long) {
        longStorage[name] = value
    }

    override fun persist(name: String, value: String) {
        stringStorage[name] = value
    }
}

@Component
class DatabasePropertiesStorage(
    private val jdbcTemplate: JdbcTemplate,
) : PropertiesStorage {
    override fun findLong(name: String): Long? {
        return jdbcTemplate.query("SELECT value FROM properties WHERE name = ?", LongResultSetExtractor, name)
    }

    override fun findString(name: String): String? {
        return jdbcTemplate.query("SELECT value FROM properties WHERE name = ?", StringResultSetExtractor, name)
    }

    override fun persist(name: String, value: Long) {
        persist(name, value.toString())
    }

    override fun persist(name: String, value: String) {
        val sql = """
            INSERT INTO properties (name, value)
            VALUES (?, ?)
            ON CONFLICT (name)
            DO UPDATE SET value = EXCLUDED.value
        """.trimIndent()
        jdbcTemplate.update(sql, name, value)
    }
}
