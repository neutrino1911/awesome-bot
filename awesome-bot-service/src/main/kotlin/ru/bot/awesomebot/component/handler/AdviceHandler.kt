package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.service.AdviceService

@Component
class AdviceHandler(private val adviceService: AdviceService) {

    @CommandMapping("/advice", description = "Получить свой совет на сегодня")
    fun handle(message: Message): String {
        val advice = adviceService.getAdvice(message.from.id)
        return wrapBlyat(advice.text)
    }

    fun wrapBlyat(text: String): String {
        val words = "(?:блять|бля|лять)"
        return text
            .replace("""(?<!,\s)\b $words\b""".toRegex(), ",$0")
            .replace("""(?<=\s|^)$words(?=\s|[^\wА-Яа-я,!?.:;]|$)""".toRegex(), "$0,")
    }
}
