package ru.bot.awesomebot.component.handler

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove
import ru.bot.awesomebot.core.dispatching.CommandMapping

@Component
class RemoveMenuHandler {
    @CommandMapping(path = ["/menu/rm"], description = "Remove reply menu")
    fun version(message: Message): SendMessage {
        return SendMessage().apply {
            chatId = message.chat.id.toString()
            replyToMessageId = message.messageId
            replyMarkup = ReplyKeyboardRemove(true)
            text = "Menu has been removed"
        }
    }
}
