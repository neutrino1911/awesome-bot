package ru.bot.awesomebot.component.handler

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import ru.bot.awesomebot.core.dispatching.CommandMapping
import ru.bot.awesomebot.core.dispatching.ParseMode.MARKDOWNV2

@Component
class StickerInfoHandler(
    objectMapper: ObjectMapper,
) {
    private val writer = objectMapper.writerWithDefaultPrettyPrinter()

    @CommandMapping("/sticker/info", description = "Получить информацию о стикере", parseMode = MARKDOWNV2)
    fun handle(message: Message): String {
        val sticker = message.replyToMessage?.sticker ?: return "\uD83D\uDCA9"
        return "```JSON\n${writer.writeValueAsString(sticker)}```"
    }
}
