package ru.bot.awesomebot.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.bot.awesomebot.data.Advice
import ru.bot.awesomebot.data.entity.AdviceHistory
import ru.bot.awesomebot.data.repository.AdviceHistoryRepository
import ru.bot.awesomebot.data.toAdvice
import ru.bot.awesomebot.service.feign.AdviceClient
import java.time.LocalDate
import java.util.concurrent.ConcurrentHashMap

@Service
class AdviceService(
    private val adviceClient: AdviceClient,
    private val adviceHistoryRepository: AdviceHistoryRepository,
) {

    private val cache: MutableMap<Int, Advice> = ConcurrentHashMap()

    @Transactional
    fun getAdvice(userId: Long): Advice {
        val today = LocalDate.now()
        adviceHistoryRepository.findByUserIdAndDate(userId, today)?.let {
            return it.toAdvice()
        }
        val advice = getAdviceOrFromCache()
        val history = AdviceHistory(adviceId = advice.id, userId = userId, date = today, text = advice.text)
        adviceHistoryRepository.save(history)
        return advice
    }

    private fun getAdviceOrFromCache(): Advice =
        try {
            val advice = adviceClient.random()
            cache.putIfAbsent(advice.id, advice)
            advice
        } catch (e: RuntimeException) {
            if (cache.isNotEmpty()) {
                val list = cache.values.toList()
                repeat(10) { list.shuffled() }
                list.random()
            } else {
                throw e
            }
        }

}
