package ru.bot.awesomebot.service

import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.w3c.dom.Element
import ru.bot.awesomebot.CurrencyNotFoundException
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.concurrent.ConcurrentHashMap
import javax.annotation.PostConstruct
import javax.xml.parsers.DocumentBuilderFactory

private val log = KotlinLogging.logger {}

@Service
class CurrencyExchangeService {

    companion object {
        private const val BASE_CUR = "EUR"
        private const val URL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
    }

    private val rates = ConcurrentHashMap<String, BigDecimal>()
    private val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
    private val builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
    private var lastUpdate: LocalDate? = null

    lateinit var ratesTime: String

    fun getRate(source: String, target: String): BigDecimal {
        ensureCurrencyExists(source, target)
        if (source == target) {
            return BigDecimal.ONE
        }
        return when (BASE_CUR) {
            source -> rates[target]!!
            target -> BigDecimal.ONE.divide(rates[source]!!, 6, RoundingMode.HALF_UP)
            else -> rates[target]!!.divide(rates[source]!!, 6, RoundingMode.HALF_UP)
        }
    }

    private fun ensureCurrencyExists(vararg currencies: String) {
        val list = ArrayList<String>()
        for (currency in currencies) {
            if (BASE_CUR != currency && !rates.containsKey(currency)) {
                list.add(currency)
            }
        }
        if (list.isNotEmpty()) {
            throw CurrencyNotFoundException(list)
        }
    }

    @PostConstruct
    @Scheduled(cron = "13 */5 16-20 * * MON-FRI", zone = "CET")
    fun init() {
        log.debug("init start")
        try {
            val today = LocalDate.now()
            if (lastUpdate != null && today.isEqual(lastUpdate)) {
                log.debug("init end 0")
                return
            }
            val doc = builder.parse(URL)
            doc.documentElement.normalize()
            val elements = doc.getElementsByTagName("Cube")
            val timeRaw = (elements.item(1) as Element).getAttribute("time")
            val time = LocalDate.parse(timeRaw, DateTimeFormatter.ISO_DATE)
            if (lastUpdate != null && !lastUpdate!!.isBefore(time)) {
                log.debug("init end 1")
                return
            }
            for (i in 0 until elements.length) {
                val element = elements.item(i) as Element
                if (element.hasAttribute("currency")) {
                    val currency = element.getAttribute("currency")
                    val rate = BigDecimal(element.getAttribute("rate"))
                    rates[currency] = rate
                }
            }
            lastUpdate = today
            ratesTime = time.format(formatter)
        } catch (e: Exception) {
            log.error(e.message, e)
        }
        log.debug("init end 2")
    }

}
