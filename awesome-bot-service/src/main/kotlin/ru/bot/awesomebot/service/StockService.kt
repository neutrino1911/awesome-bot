package ru.bot.awesomebot.service

import arrow.core.getOrElse
import arrow.core.raise.nullable
import arrow.core.raise.option
import arrow.core.recover
import arrow.core.toOption
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.telegram.telegrambots.meta.api.objects.Chat
import ru.bot.awesomebot.component.MoexClient
import ru.bot.awesomebot.data.entity.Stock
import ru.bot.awesomebot.data.entity.StockId
import ru.bot.awesomebot.data.repository.StockRepository
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.LocalDate

@Service
class StockService(
    private val stockRepository: StockRepository,
    private val moexClient: MoexClient,
) {
    private val formatter = DecimalFormat("+0.##;-0.##").apply {
        decimalFormatSymbols = DecimalFormatSymbols().apply {
            decimalSeparator = '.'
        }
    }

    @Transactional
    fun set(chat: Chat, key: String, ticker: String) {
        stockRepository.save(Stock(chatId = chat.id, key = key, ticker = ticker, figi = ""))
    }

    fun get(chat: Chat, key: String): String? {
        val stock = stockRepository.findByIdOrNull(StockId(chat.id, key)) ?: return null
        return "$key по ${formatTicker(stock.ticker)}"
    }

    fun getAliases(chat: Chat): List<String> {
        return stockRepository.findAllByChatId(chat.id).map { it.key }
    }

    @Transactional
    fun del(chat: Chat, key: String) {
        stockRepository.deleteById(StockId(chat.id, key))
    }

    private fun formatTicker(secid: String): String {
        val lastClose = option {
            val yesterday = LocalDate.now().minusDays(1)
            val response = moexClient.getHistoryPrice(secid, yesterday.minusDays(30), yesterday).bind()
            ensureNotNull(response.history.data.lastOrNull()).close
        }

        val lastPrice = nullable {
            moexClient.getCurrentPrice(secid).bind()
                .marketdata.data.lastOrNull().bind()
                .last.bind()
        }.toOption()

        return option {
            val close = lastClose.bind()
            val current = lastPrice.bind()

            val percentChange = current.minus(close).div(close)
                .movePointRight(2)
                .setScale(2, RoundingMode.HALF_UP)

            val changeString = formatter.format(percentChange)

            "$current ($changeString%)"
        }.recover {
            lastPrice.recover { lastClose.bind() }.bind().toString()
        }.getOrElse { "null" }
    }
}
