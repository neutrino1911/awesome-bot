package ru.bot.awesomebot.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.bot.awesomebot.BanException
import ru.bot.awesomebot.data.entity.StickerBan
import ru.bot.awesomebot.data.repository.StickerBanRepository

@Service
class StickerBanService(private val repository: StickerBanRepository) {

    fun isBanned(chatId: Long, key: String): Boolean {
        return repository.existsByChatIdAndKey(chatId, key)
    }

    @Transactional
    fun ban(chatId: Long, hash: String) {
        banInternal(chatId, hash)
    }

    @Transactional
    fun banPack(chatId: Long, packName: String) {
        banInternal(chatId, packName)
    }

    @Transactional
    fun unban(chatId: Long, hash: String) {
        unbanInternal(chatId, hash, "Этот стикер не в бане!")
    }

    @Transactional
    fun unbanPack(chatId: Long, key: String) {
        unbanInternal(chatId, key, "Этот пак не в бане!")
    }

    private fun banInternal(chatId: Long, key: String) {
        if (repository.existsByChatIdAndKey(chatId, key)) {
            throw BanException("Уже зобанен!")
        }
        repository.save(StickerBan(chatId = chatId, key = key))
    }

    private fun unbanInternal(chatId: Long, key: String, msg: String) {
        if (!repository.existsByChatIdAndKey(chatId, key)) {
            throw BanException(msg)
        }
        repository.deleteByChatIdAndKey(chatId, key)
    }

}
