package ru.bot.awesomebot.tools

import org.springframework.jdbc.core.ResultSetExtractor
import java.sql.ResultSet

object LongResultSetExtractor : ResultSetExtractor<Long> {
    override fun extractData(rs: ResultSet): Long? {
        return if (rs.next()) rs.getLong(1) else null
    }
}

object StringResultSetExtractor : ResultSetExtractor<String> {
    override fun extractData(rs: ResultSet): String? {
        return if (rs.next()) rs.getString(1) else null
    }
}
